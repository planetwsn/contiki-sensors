#include "lib/sensors.h"

#ifndef BH1750_H_
#define BH1750_H_

#define BH1750_ADDR		0x23
#define	BH1750_VAL		0x21

#define BH1750_PWRDWN	0x00
#define BH1750_PWRON	0x01
#define BH1750_RESET	0x07
#define CONT_HRES_1		0x10
#define CONT_HRES_2		0x11
#define CONT_LRES		0x13
#define ONET_HRES_1		0x20
#define ONET_HRES_2		0x21
#define ONET_LRES		0x23

#define BH1750_SUCCESS	0x00
#define BH1750_ERROR	(-1)

#define BH1750_ACTIVE	SENSORS_ACTIVE
#define BH1750_RESOLUTION	0x77

#define  BH1750_SENSOR 	"BH1750 Sensor"
extern const struct sensors_sensor bh1750;

#endif
