#include <stdio.h>
#include "contiki.h"
#include "dev/i2c.h"
#include "bh1750.h"
#include "lib/sensors.h"

#define DEBUG 0
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

static uint8_t enabled;

static int status (int type){
	switch(type){
		case SENSORS_ACTIVE:
		case SENSORS_READY:
			return enabled;
	}
	return 0;
}

/*
	communicates with sensor module (with data returned)

	arguments:  op -> opcode
				buf -> data receiver (storing variable)
				num -> size of data (in bytes)
*/
static uint16_t bh_read_reg(uint8_t op, uint8_t *buf, uint8_t num){
	uint8_t i;

	if((buf==NULL)||(num<=0)){
		PRINTF("BH1750: invalid buffer size\n");
		return BH1750_ERROR;
	}

	PRINTF("BH1750: enabling i2c master\n");
	i2c_master_enable();
	PRINTF("BH1750: invoking read request\n");
	if(i2c_single_send(BH1750_ADDR, op)==I2C_MASTER_ERR_NONE){
		for(i = 0; i < 180; i++){
			clock_delay_usec(1000);
		}
		//as of now we only use one of six modes available for bh1750 sensor
		//and such mode requires 180ms processing time.
		PRINTF("BH1750: retrieving results\n");
		if(i2c_burst_receive(BH1750_ADDR, buf, num) == I2C_MASTER_ERR_NONE){
			PRINTF("BH1750: data retrieval success\n");
			return BH1750_SUCCESS;
		}else{
			PRINTF("BH1750: I2C read request failed\n");
		}
	}else{
		PRINTF("BH1750: command sent failed\n");
	}

	return BH1750_ERROR;
}

/*
	communicates with sensor module (no data returned)

	arguments: op -> opcode
*/
static uint16_t bh_write_reg(uint8_t op){
	i2c_master_enable();
	if(i2c_single_send(BH1750_ADDR, op)==I2C_MASTER_ERR_NONE)
		return BH1750_SUCCESS;
	return BH1750_ERROR;
}

/**/
static int16_t data_convert(uint16_t value){
	return value/1.2;
}

/**/
static int bh_read(uint8_t op, uint16_t *rd){
	uint8_t buf[2];
	uint16_t raw;

	switch(op){
		case BH1750_RESET:
			bh_write_reg(op); break;
		case BH1750_VAL:
			PRINTF("BH1750: sending I2C command now\n");
			if(bh_read_reg(op, buf, 2)==BH1750_SUCCESS){
				raw=(buf[0]<<8)+buf[1];
				*rd=data_convert(raw);
				return BH1750_SUCCESS;
			}
	}
	PRINTF("BH1750: Invalid sensor requested!\n");
	return BH1750_ERROR;
}

static int value(int type){
	uint16_t val;

	if(type != BH1750_VAL){
		PRINTF("BH1750: Only BH1750_VAL is the accepted type now\n");
		return BH1750_ERROR;
	}

	if(!enabled){
		PRINTF("BH1750: Sensor not started!\n");
		return BH1750_ERROR;
	}
	PRINTF("BH1750: Reading sensor value\n");
	if(bh_read(type,&val)==BH1750_SUCCESS){
		return val;
	}
	return BH1750_ERROR;
}

static int configure(int type, int value){
	uint8_t buf[2];
	uint8_t i;

	if((type != BH1750_ACTIVE) && (type != BH1750_RESET) && (type != BH1750_RESOLUTION)){
		PRINTF("BH1750: Init option not supported!\n");
		return BH1750_ERROR;
	}

	switch(type){
		case BH1750_ACTIVE:
			if(value){
				i2c_init(I2C_SDA_PORT, I2C_SDA_PIN, I2C_SCL_PORT, I2C_SCL_PIN, I2C_SCL_NORMAL_BUS_SPEED);
				PRINTF("BH1750: I2C module of the firefly has been initialised. en=%i\n",value);
				enabled = value;
			}
			return BH1750_SUCCESS;
		case BH1750_RESET:
			buf[0] = BH1750_RESET;
			if(bh_write_reg(buf[0]) != BH1750_SUCCESS){
				PRINTF("BH1750: Module reset failed!\n");
				return BH1750_ERROR;
			}
			for(i=0; i<120; i++){
				clock_delay_usec(1000);
			}
			return BH1750_SUCCESS;
		case BH1750_RESOLUTION:
			PRINTF("BH1750: Resolution setup not implemented yet.\n");
			return BH1750_SUCCESS;
	}
	return BH1750_ERROR;
}

SENSORS_SENSOR(bh1750, BH1750_SENSOR, value, configure, status);
